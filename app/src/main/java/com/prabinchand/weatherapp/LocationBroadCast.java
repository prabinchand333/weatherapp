package com.prabinchand.weatherapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class LocationBroadCast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")){
            Toast.makeText(context, "Location turned on", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "location is turned off !", Toast.LENGTH_SHORT).show();
        }
    }
}
