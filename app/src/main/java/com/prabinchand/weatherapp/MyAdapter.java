package com.prabinchand.weatherapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private Context context;
    private ArrayList<WeatherModal> weatherModalArrayList;

    public MyAdapter(Context context, ArrayList<WeatherModal> weatherModalArrayList) {
        this.context = context;
        this.weatherModalArrayList = weatherModalArrayList;
    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ViewHolder holder, int position) {
        WeatherModal weatherModal = weatherModalArrayList.get(position);
        holder.temperature.setText(weatherModal.getTemperature() + "°C");
        holder.time.setText(weatherModal.getTime());
        holder.wind.setText(weatherModal.getWindSpeed());

        Picasso.get().load(weatherModal.getIcon()).into(holder.conditionImg);
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat outputDate = new SimpleDateFormat("hh:mm aa");

        try{
            Date date = inputDate.parse(weatherModal.getTime());
            holder.time.setText(outputDate.format(Objects.requireNonNull(date)));
        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return weatherModalArrayList.size();
    }




    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView time, temperature, wind;
        ImageView conditionImg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            wind = itemView.findViewById(R.id.recyclerWindSpeed);
            temperature = itemView.findViewById(R.id.recyclerViewTemperature);
            time = itemView.findViewById(R.id.recyclerViewTime);
            conditionImg = itemView.findViewById(R.id.recyclerViewConditionImg);
        }
    }
}
