package com.prabinchand.weatherapp;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    RelativeLayout rlHome;
    ProgressBar progressBar;
    TextView cityNameTV, temperatureTextView, conditionTextView, offlineTV;
    TextInputEditText cityEditTxt;
    ImageView backgroundImg, conditionViewImg, searchIcon;
    RecyclerView recyclerView;
    Button button;

    ArrayList<WeatherModal> weatherModalArrayList;
    MyAdapter myAdapter;
    private String cityName;
    double longitude;
    double latitude;

    final static String[] PERMISSION_ALL = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private final int PERMISSION_CODE = 1;
    FusedLocationProviderClient fusedLocationProviderClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.activity_main);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        requestForLocationPermission();
        checkLocationIsEnabled();
        findViewByIDS();

        registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.MODE_CHANGED_ACTION));

        isInternetAvailable();
        if (isInternetAvailable()) {
            getLastLocation();
            hideInputKeyboard();

        } else {
            offlineTV.setVisibility(View.VISIBLE);
            button.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isInternetAvailable()) {
                        offlineTV.setVisibility(View.GONE);
                        button.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                        getLastLocation();
                        hideInputKeyboard();
                    } else {
                        Toast.makeText(MainActivity.this, "Connect to internet to use this app", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        weatherModalArrayList = new ArrayList<>();
        myAdapter = new MyAdapter(this, weatherModalArrayList);
        recyclerView.setAdapter(myAdapter);

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String city = Objects.requireNonNull(cityEditTxt.getText()).toString();
                if (city.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Please enter city name", Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < 720; i++) {
                        searchIcon.animate().rotation(i).setDuration(700);
                    }
                    cityNameTV.setText(cityName);
                    getWeatherInfo(city);
                }
            }
        });
    }

    private void findViewByIDS() {

        offlineTV = findViewById(R.id.offlineTextView);
        button = findViewById(R.id.retryBtn);
        rlHome = findViewById(R.id.relativeLayoutHome);
        progressBar = findViewById(R.id.progressBar);
        cityNameTV = findViewById(R.id.textViewCityName);
        temperatureTextView = findViewById(R.id.textViewTemperature);
        conditionTextView = findViewById(R.id.textViewCondition);
        cityEditTxt = findViewById(R.id.textEditCityName);
        backgroundImg = findViewById(R.id.imageViewBackground);
        conditionViewImg = findViewById(R.id.imageViewCondition);
        searchIcon = findViewById(R.id.imageViewSearchIcon);
        recyclerView = findViewById(R.id.recyclerView);
    }

    private boolean isInternetAvailable() {
        CheckInternetAccess checkInternetAccess = new CheckInternetAccess(this);
        return checkInternetAccess.isNetworkConnection();
    }

    private void hideInputKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                                try {
                                    List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 10);
                                    if (addresses != null && !addresses.isEmpty()) {
                                        cityName = addresses.get(0).getLocality();
                                        getWeatherInfo(cityName);
                                    } else {
                                        Toast.makeText(MainActivity.this, "GeoCoder : Not able to fetch cityName", Toast.LENGTH_SHORT).show();
                                    }


                                } catch (IOException e) {
                                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(MainActivity.this, "location_NULL : " + location.getLatitude() + "  " + location.getLongitude(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    private void checkLocationIsEnabled() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled = false;
        boolean networkEnabled = false;

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!gpsEnabled && !networkEnabled) {
            new AlertDialog.Builder(this)
                    .setTitle("Enable GPS Service")
                    .setCancelable(false)
                    .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(MainActivity.this, "You need to enable GPS to use this app", Toast.LENGTH_SHORT).show();
                        }
                    }).show();
        }
    }

    private void requestForLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, PERMISSION_ALL, PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Please provide the permission", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private String getCityName(double latitude, double longitude) {
        String cityName = "-/-";
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addressList = gcd.getFromLocation(latitude, longitude, 10);
            for (Address address : addressList) {
                if (address != null) {
                    String city = address.getLocality();
                    if (city != null && !city.equals("")) {
                        cityName = city;
                    } else {
                        Toast.makeText(this, "City name not found !", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (IOException e) {
            Toast.makeText(this, "Error : " + e, Toast.LENGTH_SHORT).show();
        }
        return cityName;
    }

    private void getWeatherInfo(String cityName) {
        String url = "https://api.openweathermap.org/data/2.5/forecast?q=" + cityName + "&appid=631ff1edc5ab74936416305c6d3aa7d4";
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @SuppressLint({"SetTextI18n", "NotifyDataSetChanged"})
            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);
                rlHome.setVisibility(View.VISIBLE);
                weatherModalArrayList.clear();

                try {
                    String temp = response.getJSONArray("list").getJSONObject(0).getJSONObject("main").getString("temp");
                    temperatureTextView.setText("");
                    double temperatureDegree = (Double.parseDouble(temp) - 273.15);
                    int finalTemp = (int) temperatureDegree;
                    temperatureTextView.setText(finalTemp + "°C");

                    String condition = response.getJSONArray("list").getJSONObject(0).getJSONArray("weather").getJSONObject(0).getString("description");
                    conditionTextView.setText(condition);

                    String weatherIcon = response.getJSONArray("list").getJSONObject(0).getJSONArray("weather").getJSONObject(0).getString("icon");
                    String weatherUrl = "http://openweathermap.org/img/w/" + weatherIcon + ".png";

                    Picasso.get().load(weatherUrl).into(conditionViewImg);
                    cityNameTV.setText(cityName);

                    String isDay = response.getJSONArray("list").getJSONObject(0).getJSONObject("sys").getString("pod");
                    if (isDay.equals("n")) {
                        Picasso.get().load(R.drawable.night).into(backgroundImg);
                    } else {
                        Picasso.get().load(R.drawable.day).into(backgroundImg);
                    }

                    JSONArray hourArray = response.getJSONArray("list");
                    for (int i = 0; i < 6; i++) {
                        JSONObject hourObj = hourArray.getJSONObject(i);
                        String time = hourObj.getString("dt_txt");

                        String temper = hourObj.getJSONObject("main").getString("temp");
                        double temperInCelsius = (Double.parseDouble(temper) - 273.15);
                        int tempInt = (int) temperInCelsius;
                        String finalTempRecyclerview = String.valueOf(tempInt);

                        String img = hourObj.getJSONArray("weather").getJSONObject(0).getString("icon");
                        String imgURL = "http://openweathermap.org/img/w/" + img + ".png";

                        String wind = hourObj.getJSONObject("wind").getString("speed") + " km/h";
                        searchIcon.setRotation(0);
                        weatherModalArrayList.add(new WeatherModal(time, finalTempRecyclerview, imgURL, wind));
                        cityEditTxt.setText("");

                    }
                    myAdapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    Toast.makeText(MainActivity.this, "Something went wrong. Try again", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Please provide correct city or country name", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    private final BroadcastReceiver mGpsSwitchStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(intent.getAction())) {
                Toast.makeText(MainActivity.this, "location enabled ", Toast.LENGTH_SHORT).show();
//                startActivity(getIntent());
                getLastLocation();
            } else {
                Toast.makeText(MainActivity.this, "Location is disabled", Toast.LENGTH_SHORT).show();
            }
        }
    };


}